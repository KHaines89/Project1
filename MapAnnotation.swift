//
//  MapAnnotation.swift
//  Project 1
//
//  Created by Kevin Daniel Haines on 9/21/16.
//  Copyright © 2016 Kevin Daniel Haines. All rights reserved.
//

import MapKit

class MapAnnotation: NSObject, MKAnnotation{
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    
    init(title: String, subtitle:String, coordinate:CLLocationCoordinate2D){
        self.title = title
        self.subtitle = subtitle
        self.coordinate = coordinate
        
    }
}
