//
//  ViewController.swift
//  Project 1
//
//  Created by Kevin Daniel Haines on 9/21/16.
//  Copyright © 2016 Kevin Daniel Haines. All rights reserved.
//

import UIKit
import MapKit
import Alamofire
import CoreLocation


class ViewController: UIViewController, CLLocationManagerDelegate {

    var locationManager: CLLocationManager?
    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let boiseLocation:CLLocationCoordinate2D = CLLocationCoordinate2DMake(43.615431, -116.203622)
        
        //let newAnnotation = MapAnnotation(title: "Pin Title", subtitle: "Location name", coordinate: boiseLocation)
        
      //  mapView.addAnnotation( newAnnotation )
        
        mapView.setRegion(MKCoordinateRegionMakeWithDistance(boiseLocation, 7000, 5000), animated: true)
        mapView.mapType = MKMapType.standard
        
        Alamofire.request("https://s3-us-west-2.amazonaws.com/electronic-armory/buildings.json").responseString{ response in
            print(response.request)  // original URL request
            print(response.response) // HTTP URL response
            print(response.data)     // server data
            print(response.result)
            
            do{
                let stringResponse = response.result.value!
                let data = stringResponse.data(using: .utf8)!
                
                let jsonResult:NSArray = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as! NSArray
                
                for (index, locationObject) in jsonResult.enumerated(){
                    let dict = locationObject as! Dictionary<String, AnyObject>
                    print(dict["name"])
                    print(dict["description"])
                    print(dict["location"]?["latitude"])
                    print(dict["location"]?["longitude"])
                    
                    let currentLocation:CLLocationCoordinate2D = CLLocationCoordinate2DMake(dict["location"]?["latitude"] as! CLLocationDegrees, dict["location"]?["longitude"] as! CLLocationDegrees)
                    
                    let newAnnotation = MapAnnotation(title: dict["name"] as! String, subtitle: dict["description"] as! String, coordinate: currentLocation)
                    
                    self.mapView.addAnnotation(newAnnotation)
                }
            }
            catch{
                print("error")
            }
        }
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.requestAlwaysAuthorization()
        locationManager?.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locationManager?.distanceFilter = 100.0
        locationManager?.startUpdatingLocation()
    }
    func locationManager(_ manager:CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus){
        if status == .authorizedAlways{
            if CLLocationManager.isMonitoringAvailable(for: CLBeaconRegion.self){
                if CLLocationManager.isRangingAvailable(){
                    
                }
            }
        }
        
    }
    func locationManager(_ manager:CLLocationManager, didFailWithError error: Error){
        print(error)
        
    }
    func locationManager(_ manager:CLLocationManager, didUpdateLocations locations: [CLLocation]){
        print(locations)
        //mapView.setRegion(MKCoordinateRegionMakeWithDistance(locations[0].coordinate, 7000, 5000), animated: true)

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let detailsViewController:DetailsViewController = segue.destination as! DetailsViewController
        detailsViewController.mapView = self.mapView
    }
    
}


