//
//  DetailsViewController.swift
//  Project 1
//
//  Created by Kevin Daniel Haines on 9/21/16.
//  Copyright © 2016 Kevin Daniel Haines. All rights reserved.
//
import UIKit
import MapKit
import CoreLocation


class DetailsViewController: UIViewController, CLLocationManagerDelegate{
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var descriptionTextField: UITextField!
    var mapView:MKMapView?
    
    var locationManager:CLLocationManager?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager = CLLocationManager()
        locationManager?.delegate = self
    }
    
    @IBAction func saveLocation(_ sender: AnyObject) {
        let title: String = titleTextField.text!
        let description: String  = descriptionTextField.text!
        let mapPin:MapAnnotation = MapAnnotation(title: titleTextField.text!, subtitle: descriptionTextField.text!, coordinate:(mapView?.userLocation.coordinate)!)
        mapView?.addAnnotation(mapPin)
        self.dismiss(animated: true, completion: nil)
    }
}
